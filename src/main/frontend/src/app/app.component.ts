import {Component, Inject} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from '@angular/material/dialog';
import {AuthService} from './service/auth.service';

export interface DialogData {
  username: string;
  password: string;
}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'elte-movie-app';
  username: string;
  password: string;

  constructor(
    public dialog: MatDialog,
    public authService: AuthService
  ) {
  }

  openLoginDialog(): void {
    const dialogRef = this.dialog.open(LoginDialogComponent, {
      autoFocus: false,
      width: '350px',
      height: 'auto',
      data: {username: this.username, password: this.password}
    });

    dialogRef.afterClosed().subscribe(result => {
    });
  }

  openRegisterDialog(): void {
    const dialogRef = this.dialog.open(RegisterDialogComponent, {
      autoFocus: false,
      width: '350px',
      height: 'auto',
      data: {username: this.username, password: this.password}
    });

    dialogRef.afterClosed().subscribe(result => {

    });
  }

  logout() {
    this.authService.logout();
  }
}

@Component({
  selector: 'app-login-dialog',
  templateUrl: 'login-dialog.html',
  styleUrls: ['./app.component.scss']
})
export class LoginDialogComponent {
  loginError = false;

  constructor(
    public dialogRef: MatDialogRef<LoginDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData,
    private authService: AuthService
  ) {
  }

  onLoginClick(username: string, password: string) {
    this.data.username = '';
    this.data.password = '';

    this.authService.login(username, password).subscribe( res => {
      this.authService.username = username;
      this.authService.isLoggedIn = true;
      this.dialogRef.close();
    }, error => {
      this.loginError = true;
    });
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

}

@Component({
  selector: 'app-register-dialog',
  templateUrl: 'register-dialog.html',
  styleUrls: ['./app.component.scss']
})
export class RegisterDialogComponent {
  registerSuccess = false;
  registerError = false;

  constructor(
    public dialogRef: MatDialogRef<RegisterDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData,
    private authService: AuthService
  ) {
  }

  onRegisterClick(username: string, password: string) {
    this.data.username = '';
    this.data.password = '';
    this.authService.register(username, password).subscribe(
      data => {
        this.registerSuccess = true;
      },
      error => {
        this.registerError = true;
      }
    );
    this.registerSuccess = false;
    this.registerError = false;
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

}
