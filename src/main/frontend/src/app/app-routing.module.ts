import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {HomeComponent} from './components/home/home.component';
import {ReservationComponent} from './components/reservation/reservation.component';
import {ProfilComponent} from './components/profil/profil.component';
import {AuthGuardService} from './service/auth-guard.service';

const routes: Routes = [
  {
    path: 'home',
    component: HomeComponent
  },
  {
    path: 'reservations',
    component: ReservationComponent,
    canActivate: [AuthGuardService]
  },
  {
    path: 'profile',
    component: ProfilComponent,
    canActivate: [AuthGuardService]
  },
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  },
  {
    path: '**',
    redirectTo: '/'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
