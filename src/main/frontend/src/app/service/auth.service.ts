import {Injectable} from '@angular/core';
import {HttpService} from './http.service';
import {BehaviorSubject} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  isLoggedIn = false;
  username: string;

  constructor(
    private httpService: HttpService
  ) {
  }

  register(username: string, password: string) {
    return this.httpService.postUser(username, password);
  }

  login(username: string, password: string) {
    return this.httpService.login(username, password);
  }

  logout() {
    this.username = undefined;
    this.isLoggedIn = false;
    localStorage.setItem('token', '');
  }

}
