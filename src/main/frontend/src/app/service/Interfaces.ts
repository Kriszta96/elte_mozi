export interface User {
  username: string;
  password: string;
  role: string;
}

export interface Movie {
  title: string;
  releaseYear: number;
  link: string;
}
