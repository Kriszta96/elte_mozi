import {Injectable} from '@angular/core';
import {Movie} from './Interfaces';

@Injectable({
  providedIn: 'root'
})
export class MovieService {

  movies: Movie[] = [
    {title: 'Oroszlánkirály', releaseYear: 2018, link: 'https://static.mozipremierek.hu/poster/1636-az-oroszlankiraly.41164.jpg'},
    {title: 'Harry Potter', releaseYear: 2008, link: 'https://m.blog.hu/fi/filmek/image/2010/harry_potter_7/harry_potter_7_poster_20.jpg'},
    {title: 'Alkonyat', releaseYear: 2012, link: 'https://static.posters.cz/image/750/plakatok/twilight-i5991.jpg'},
    {title: 'Alkonyat', releaseYear: 2012, link: 'https://static.posters.cz/image/750/plakatok/twilight-i5991.jpg'},
    {title: 'Alkonyat', releaseYear: 2012, link: 'https://static.posters.cz/image/750/plakatok/twilight-i5991.jpg'},
    {title: 'Alkonyat', releaseYear: 2012, link: 'https://static.posters.cz/image/750/plakatok/twilight-i5991.jpg'},
    {title: 'Alkonyat', releaseYear: 2012, link: 'https://static.posters.cz/image/750/plakatok/twilight-i5991.jpg'},
    {title: 'Alkonyat', releaseYear: 2012, link: 'https://static.posters.cz/image/750/plakatok/twilight-i5991.jpg'},


  ];

  constructor() {
  }

  getMovies() {
    return this.movies;
  }
}
