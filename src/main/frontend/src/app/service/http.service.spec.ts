import {inject, TestBed} from '@angular/core/testing';
import {HttpService} from './http.service';
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';

describe('HttpService', () => {
  let service: HttpService;
  let httpTestingController: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [HttpService]
    });
    httpTestingController = TestBed.inject(HttpTestingController);
    service = TestBed.inject(HttpService);
  });

  afterEach(inject([HttpTestingController], (httpMock: HttpTestingController) => {
    httpMock.verify();
  }));

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('#postUser should return new User',
    inject([HttpTestingController, HttpService],
      (httpMock: HttpTestingController, service: HttpService) => {
        const mockUserResponse = {
          username: 'test',
          password: 'password'
        };

        service.postUser('test', 'password').subscribe(user => {
          expect(user.username).toBe('test');
          expect(user.password).toBe('password');
        });

        const req = httpMock.expectOne('/api/users');
        expect(req.request.method).toEqual('POST');
        req.flush(mockUserResponse);
      })
  );

  it('#login should return User',
    inject([HttpTestingController, HttpService],
      (httpMock: HttpTestingController, service: HttpService) => {
        const mockUserResponse = {
          username: 'test',
          password: 'password'
        };

        service.login('test', 'password').subscribe(user => {
          expect(user.username).toBe('test');
          expect(user.password).toBe('password');
        });

        const req = httpMock.expectOne('/api/users/login');
        expect(req.request.method).toEqual('POST');
        req.flush(mockUserResponse);
      })
  );
});

