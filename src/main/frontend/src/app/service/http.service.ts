import { Injectable } from '@angular/core';
import {User} from './Interfaces';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class HttpService {

  private get options() {
    const headers = {
      'Content-Type': 'application/json',
      'X-Requested-With': 'XMLHttpRequest',
      Authorization: undefined
    };

    if (window.localStorage.getItem('token')) {
      headers.Authorization = `Basic ${window.localStorage.getItem('token')}`;
    }

    return {
      headers: new HttpHeaders(headers)
    };
  }

  constructor(
    private http: HttpClient
  ) { }

  postUser(username: string, password: string) {
    const user: User = {username, password, role: 'USER'};
    return this.http.post<User>('/api/users', user);
  }

  login(username: string, password: string): Observable<any> {
    try {
      const token = btoa(username + ':' + password);
      window.localStorage.setItem('token', token);
      return this.http.post('/api/users/login', username, this.options);
    } catch (e) {
      window.localStorage.setItem('token', '');
    }
  }

}
