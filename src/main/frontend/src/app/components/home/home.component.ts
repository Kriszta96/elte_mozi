import { Component, OnInit } from '@angular/core';
import {MovieService} from '../../service/movie.service';
import {Movie} from '../../service/Interfaces';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  movies: Movie[];

  constructor(
    public movieService: MovieService
  ) { }

  ngOnInit(): void {
    this.movies = this.movieService.getMovies();
  }

}
