import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HomeComponent } from './home.component';

describe('HomeComponent', () => {
  let component: HomeComponent;
  let fixture: ComponentFixture<HomeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HomeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('movie array has values after create', () => {
    expect(component.movies.length).toBeGreaterThanOrEqual(0);
  });

  it('a.href should be a string', () => {
    fixture.detectChanges();
    const a = fixture.nativeElement.querySelector('a');
    expect(a.href).toBeInstanceOf(String);
  });

  it('h5 should should be a string', () => {
    fixture.detectChanges();
    const h5 = fixture.nativeElement.querySelector('h5');
    expect(h5.textContent).toBeInstanceOf(String);
  });
});
