import {async, TestBed} from '@angular/core/testing';
import {AppComponent, LoginDialogComponent, RegisterDialogComponent} from './app.component';
import {MatDialog, MatDialogModule} from '@angular/material/dialog';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {MatMenuModule} from '@angular/material/menu';
import {of} from 'rxjs';

describe('AppComponent', () => {
  let registerDialogService: AppComponent;
  let loginDialogService: AppComponent;
  let dialogSpy: jasmine.Spy;
  const dialogRefSpyObj = jasmine.createSpyObj({afterClosed: of({}), close: null});
  dialogRefSpyObj.componentInstance = {body: ''};

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [MatDialogModule, HttpClientTestingModule, MatMenuModule],
      declarations: [
        AppComponent
      ],
      providers: [AppComponent]
    }).compileComponents();
    registerDialogService = TestBed.inject(AppComponent);
    loginDialogService = TestBed.inject(AppComponent);
  }));

  beforeEach(() => {
    dialogSpy = spyOn(TestBed.inject(MatDialog), 'open').and.returnValue(dialogRefSpyObj);
  });

  it('should create the app', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.componentInstance;
    expect(app).toBeTruthy();
  });

  it('open register modal', () => {
    registerDialogService.openRegisterDialog();
    expect(dialogSpy).toHaveBeenCalled();

    expect(dialogSpy).toHaveBeenCalledWith(
      RegisterDialogComponent,
      {
        autoFocus: false, width: '350px', height: 'auto', data: Object({username: undefined, password: undefined})
      });

    expect(dialogRefSpyObj.afterClosed).toHaveBeenCalled();
  });

  it('open login modal', () => {
    loginDialogService.openLoginDialog();
    expect(dialogSpy).toHaveBeenCalled();

    expect(dialogSpy).toHaveBeenCalledWith(
      LoginDialogComponent,
      {
        autoFocus: false, width: '350px', height: 'auto', data: Object({username: undefined, password: undefined})
      });

    expect(dialogRefSpyObj.afterClosed).toHaveBeenCalled();
  });
});
