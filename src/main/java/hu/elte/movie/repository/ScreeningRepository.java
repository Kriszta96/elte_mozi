package hu.elte.movie.repository;

import hu.elte.movie.model.Screening;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ScreeningRepository extends CrudRepository<Screening, Integer> {
    Optional<Screening> findById(Integer id);
    Iterable<Screening> findAll();
    Iterable<Screening> findByMovieId(Integer movieId);
    Iterable<Screening> findByRoomId(Integer roomId);
    Iterable<Screening> findByTime(String time);
}
