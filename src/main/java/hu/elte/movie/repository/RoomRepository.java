package hu.elte.movie.repository;

import hu.elte.movie.model.Room;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface RoomRepository extends CrudRepository<Room, Integer> {
    Optional<Room> findById(Integer id);
    Iterable<Room> findAll();
    Iterable<Room> findByName(String name);
}
