package hu.elte.movie.repository;

import hu.elte.movie.model.Movie;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface MovieRepository extends CrudRepository<Movie, Integer> {
    Optional<Movie> findById(Integer id);
    List<Movie> findAll();
    Iterable<Movie> findByTitle(String title);
    Iterable<Movie> findByReleaseYear(String title);
}
