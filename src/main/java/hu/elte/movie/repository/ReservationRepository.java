package hu.elte.movie.repository;

import hu.elte.movie.model.Reservation;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ReservationRepository extends CrudRepository<Reservation, Integer> {
    Optional<Reservation> findById(Integer id);
    Iterable<Reservation> findAll();
    Iterable<Reservation> findByUserId(Integer userId);
    Iterable<Reservation> findByScreeningId(Integer screeningId);

}
