package hu.elte.movie.repository;


import hu.elte.movie.model.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserRepository extends CrudRepository<User, Integer> {
    Optional<User> findById(Integer id);
    Iterable<User> findAll();
    Optional<User> findByUsername(String username);
    Iterable<User> findByRole(User.Role role);
}
