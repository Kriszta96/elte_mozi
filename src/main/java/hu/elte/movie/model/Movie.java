package hu.elte.movie.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Getter;
import lombok.Data;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.EqualsAndHashCode;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Column;
import javax.persistence.OneToMany;
import javax.persistence.CascadeType;

import java.io.Serializable;
import java.util.List;


@Getter
@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
public class Movie implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column
    private Integer id;

    @Column(nullable = false)
    private String title;

    @Column
    private String releaseYear;

    @OneToMany(mappedBy = "movie", cascade = CascadeType.REMOVE)
    @JsonIgnore
    private List<Screening> screenings;

    /**
     * Constructor with the id included
     * @param id the movie's id
     * @param title the movie's title
     * @param releaseYear the movie's release year
     */
    public Movie(int id, String title, String releaseYear) {
        this.id = id;
        this.title = title;
        this.releaseYear = releaseYear;
    }

    /**
     * Constructor without the id
     * @param title the movie's title
     * @param releaseYear the movie's release year
     */
    public Movie(String title, String releaseYear) {
        this.title = title;
        this.releaseYear = releaseYear;
    }

    public Integer getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getReleaseYear() {
        return releaseYear;
    }
}

