package hu.elte.movie.controller;


import hu.elte.movie.model.Room;
import hu.elte.movie.repository.RoomRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.DeleteMapping;

import java.util.Optional;

@CrossOrigin
@RestController
@RequestMapping("/api/rooms")
public class RoomController {
    @Autowired
    private RoomRepository roomRepository;

    /**
     * Getting the room back by the given id.
     * @param id
     * @return
     */
    @GetMapping("/{id}")
    public Optional<Room> getRoomById(
            @PathVariable Integer id
    ) {
        return roomRepository.findById(id);
    }

    /**
     * Getting all the rooms back.
     * @return
     */
    @GetMapping("")
    public Iterable<Room> getRoom() {
        return roomRepository.findAll();
    }

    /**
     * Getting a room back by the given name.
     * @param name
     * @return
     */
    @GetMapping("/Name/{name}")
    public Iterable<Room> getRoomByNameId(
            @PathVariable String name
    ) {
        return roomRepository.findByName(name);
    }

    /**
     * Creating a new room.
     * @param room
     * @return
     */
    @PostMapping("")
    public ResponseEntity<Room> createRoom(
            @RequestBody Room room
    ) {
        Room savedRoom = roomRepository.save(room);
        return ResponseEntity.ok(savedRoom);
    }

    /**
     * Deleting an existing room by the given id.
     * @param id
     * @return
     */
    @DeleteMapping("/{id}")
    public ResponseEntity<Room> deleteRoom(
            @PathVariable Integer id
    ) {
        try {
            roomRepository.deleteById(id);
            return ResponseEntity.ok().build();
        } catch (Exception e) {
            return ResponseEntity.notFound().build();
        }
    }
}

