package hu.elte.movie.controller;

import hu.elte.movie.model.Reservation;
import hu.elte.movie.repository.ReservationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.DeleteMapping;

import java.util.Optional;

@CrossOrigin
@RestController
@RequestMapping("/api/reservations")
public class ReservationController {
    @Autowired
    private ReservationRepository reservationRepository;

    /**
     * Returning a reservation by the given id.
     * @param id
     * @return
     */
    @GetMapping("/{id}")
    public Optional<Reservation> getReservationById(
            @PathVariable Integer id
    ) {
        return reservationRepository.findById(id);
    }

    /**
     * Getting all the reservations back.
     * @return
     */
    @GetMapping("")
    public Iterable<Reservation> getReservations() {
        return reservationRepository.findAll();
    }

    /**
     * Returning a reservation by the given user's id.
     * @param userId
     * @return
     */
    @GetMapping("/userId/{userId}")
    public Iterable<Reservation> getReservationByUserId(
            @PathVariable Integer userId
    ) {
        return reservationRepository.findByUserId(userId);
    }

    /**
     * Returning a reservation by the given screening's id.
     * @param screeningId
     * @return
     */
    @GetMapping("/screeningId/{screeningId}")
    public Iterable<Reservation> getReservationByScreeningId(
            @PathVariable Integer screeningId
    ) {
        return reservationRepository.findByScreeningId(screeningId);
    }


    /**
     * Creating a new reservation.
     * @param reservation
     * @return
     */
    @PostMapping("")
    public ResponseEntity<Reservation> createReservation(
            @RequestBody Reservation reservation
    ) {
        Reservation savedReservation = reservationRepository.save(reservation);
        return ResponseEntity.ok(savedReservation);
    }

    /**
     * Deleting an existing reservation by the given id.
     * @param id
     * @return
     */
    @DeleteMapping("/{id}")
    public ResponseEntity<Reservation> deleteReservation(
            @PathVariable Integer id
    ) {
        try {
            reservationRepository.deleteById(id);
            return ResponseEntity.ok().build();
        } catch (Exception e) {
            return ResponseEntity.notFound().build();
        }
    }
}

