package hu.elte.movie.controller;

import hu.elte.movie.model.User;
import hu.elte.movie.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PutMapping;
import java.util.Optional;

@CrossOrigin
@RestController
@RequestMapping("/api/users")
public class UserController {

    public static final int NOT_FOUND = 404;
    public static final int BAD_REQUEST = 400;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    /**
     * Getting an user back by the user's id.
     * @param id
     * @return
     */
    @GetMapping("/{id}")
    public Optional<User> getUserById(
            @PathVariable Integer id
    ) {
        return userRepository.findById(id);
    }

    /**
     * All users will be listed.
     * @return
     */
    @GetMapping("")
    public Iterable<User> getUsers() {
        return userRepository.findAll();
    }

    /**
     * Getting an user back by the given username.
     * @param username
     * @return
     */
    @GetMapping("/username/{username}")
    public ResponseEntity getUserByUsername(
            @PathVariable String username
    ) {
        Optional<User> oUser = userRepository.findByUsername(username);
        if (!oUser.isPresent()) {
            return ResponseEntity.status(NOT_FOUND).build();
        }
        return ResponseEntity.ok(oUser.get());
    }

    /**
     * The login method for the given user.
     * @param username
     * @return
     */
    @PostMapping("/login")
    public ResponseEntity<User> login(@RequestBody String username) {
        Optional<User> oUser =
                userRepository.findByUsername(username);
        if (!oUser.isPresent()) {
            return ResponseEntity.status(BAD_REQUEST).build();
        }
        System.out.println(oUser.get());
        return ResponseEntity.ok(oUser.get());

    }

    /**
     * Creating a new user.
     * @param user
     * @return
     */
    @PostMapping("")
    public ResponseEntity<User> createUser(@RequestBody User user) {
        System.out.println(getUserByUsername(user.getUsername()));
        if (getUserByUsername(user.getUsername()).getStatusCode().value() != NOT_FOUND) {
            return ResponseEntity.badRequest()
                    .build();
        }
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        userRepository.save(user);
        return ResponseEntity.ok(user);
    }

    /**
     * Modyfing the user by the user's id.
     * @param id
     * @param user
     * @return
     */
    @PutMapping("/{id}")
    public ResponseEntity<User> modifyUser(
            @PathVariable Integer id,
            @RequestBody User user
    ) {
        Optional<User> oUser = userRepository.findById(id);
        Optional<User> oExistingUser = userRepository.findByUsername(user.getUsername());
        if (oUser.isPresent() && !oExistingUser.isPresent()) {
            if (user.getUsername() == null && user.getPassword() == null && user.getRole() == null) {
                return ResponseEntity.badRequest().build();
            }
            User oldUser = oUser.get();

            oldUser.setUsername(user.getUsername());
            oldUser.setPassword(user.getPassword());

            User savedUser = userRepository.save(oldUser);
            return ResponseEntity.ok(savedUser);
        } else {
            return ResponseEntity.badRequest().build();
        }
    }

    /**
     * Deleting an existing user.
     * @param id
     * @return
     */
    @DeleteMapping("/{id}")
    public ResponseEntity<User> deleteUser(
            @PathVariable Integer id
    ) {
        try {
            userRepository.deleteById(id);
            return ResponseEntity.ok().build();
        } catch (Exception e) {
            return ResponseEntity.notFound().build();
        }
    }
}
