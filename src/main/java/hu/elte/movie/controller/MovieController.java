package hu.elte.movie.controller;

import hu.elte.movie.model.Movie;
import hu.elte.movie.repository.MovieRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.DeleteMapping;



import java.util.Optional;

@CrossOrigin
@RestController
@RequestMapping("/api/movies")
public class MovieController {

    private final MovieRepository movieRepository;

    public MovieController(MovieRepository movieRepository) {
        this.movieRepository = movieRepository;
    }

    /**
     * Getting the movie back by the given id.
     * @param id
     * @return
     */
    @GetMapping("/{id}")
    public Optional<Movie> getMovieById(
            @PathVariable Integer id
    ) {
        return movieRepository.findById(id);
    }

    /**
     * Getting all the movies back.
     * @return
     */
    @GetMapping("")
    public Iterable<Movie> getMovies() {
        return movieRepository.findAll();
    }

    /**
     * Getting a movie back by the given title.
     * @param title
     * @return
     */
    @GetMapping("/title/{title}")
    public Iterable<Movie> getMovieByTitle(
            @PathVariable String title
    ) {
        return movieRepository.findByTitle(title);
    }

    /**
     * Getting a movie back by the given release year.
     * @param releaseYear
     * @return
     */
    @GetMapping("/release-year/{releaseYear}")
    public Iterable<Movie> getMovieByReleaseYear(
            @PathVariable String releaseYear
    ) {
        return movieRepository.findByReleaseYear(releaseYear);
    }

    /**
     * Creating a new movie.
     * @param movie
     * @return
     */
    @PostMapping("")
    public ResponseEntity<Movie> createMovie(
            @RequestBody Movie movie
    ) {
        Movie savedMovie = movieRepository.save(movie);
        return ResponseEntity.ok(savedMovie);
    }

    /**
     * Deleting an existing movie by the given id.
     * @param id
     * @return
     */
    @DeleteMapping("/{id}")
    public ResponseEntity deleteMovie(
            @PathVariable Integer id
    ) {
        try {
            movieRepository.deleteById(id);
            return ResponseEntity.ok().build();
        } catch (Exception e) {
            return ResponseEntity.notFound().build();
        }
    }
}
