package hu.elte.movie.controller;

import hu.elte.movie.model.Screening;
import hu.elte.movie.repository.ScreeningRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.DeleteMapping;

import java.util.Optional;

@CrossOrigin
@RestController
@RequestMapping("/api/screenings")
public class ScreeningController {
    @Autowired
    private ScreeningRepository screeningRepository;

    /**
     * Getting a screening back by the given id.
     * @param id
     * @return
     */
    @GetMapping("/{id}")
    public Optional<Screening> getScreeningById(
            @PathVariable Integer id
    ) {
        return screeningRepository.findById(id);
    }

    /**
     * Getting all the screenings back.
     * @return
     */
    @GetMapping("")
    public Iterable<Screening> getScreenings() {
        return screeningRepository.findAll();
    }

    /**
     * Getting a screening back by the given movie's id.
     * @param movieId
     * @return
     */
    @GetMapping("/movieId/{movieId}")
    public Iterable<Screening> getScreeningByMovieId(
            @PathVariable Integer movieId
    ) {
        return screeningRepository.findByMovieId(movieId);
    }


    /**
     * Getting a screening back by the given room's id.
     * @param roomId
     * @return
     */
    @GetMapping("/roomID/{roomId}")
    public Iterable<Screening> getScreeningByRoomId(
            @PathVariable Integer roomId
    ) {
        return screeningRepository.findByRoomId(roomId);
    }

    /**
     * Getting a screening back by the given time.
     * @param time
     * @return
     */
    @GetMapping("/time/{time}")
    public Iterable<Screening> getScreeningByTime(
            @PathVariable String time
    ) {
        return screeningRepository.findByTime(time);
    }

    /**
     * Creating a screening.
     * @param screening
     * @return
     */
    @PostMapping("")
    public ResponseEntity<Screening> createScreening(
            @RequestBody Screening screening
    ) {
        Screening savedScreening = screeningRepository.save(screening);
        return ResponseEntity.ok(savedScreening);
    }

    /**
     * Deleting an existing screening by the given id.
     * @param id
     * @return
     */
    @DeleteMapping("/{id}")
    public ResponseEntity<Screening> deleteScreening(
            @PathVariable Integer id
    ) {
        try {
            screeningRepository.deleteById(id);
            return ResponseEntity.ok().build();
        } catch (Exception e) {
            return ResponseEntity.notFound().build();
        }
    }
}

