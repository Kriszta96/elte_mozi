package hu.elte.movie.unit;

import hu.elte.movie.controller.RoomController;
import hu.elte.movie.model.*;
import io.restassured.http.ContentType;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.PostConstruct;
import java.util.ArrayList;

import static io.restassured.RestAssured.*;
import static io.restassured.RestAssured.delete;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@RunWith(SpringRunner.class)
public class RoomControllerTest {
    @LocalServerPort
    private int port;

    private String uri;

    @PostConstruct
    public void init() {
        uri = "http://localhost:" + port;
    }

    @MockBean
    RoomController roomController;

    @Test
    public void getAllRooms() {

        ArrayList<Room> roomArray = new ArrayList<>();
        Room room1 = new Room(1, "room1");
        Room room2 = new Room(1, "room2");
        Room room3 = new Room(1, "room3");
        Room room4 = new Room(1, "room4");
        roomArray.add(room1);
        roomArray.add(room2);
        roomArray.add(room3);
        roomArray.add(room4);

        when(roomController.getRoom()).thenReturn(roomArray);

        Room[] rooms = get(uri + "/api/rooms").then()
                .statusCode(200)
                .extract()
                .as(Room[].class);
        assertEquals(rooms.length, 4);
    }

    @Test
    public void getRoomById() {

        Room room1 = new Room(1, "room1");

        when(roomController.getRoomById(1)).thenReturn(java.util.Optional.of(room1));

        get(uri + "/api/rooms/1").then()
                .assertThat()
                .statusCode(HttpStatus.OK.value())
                .body("id", equalTo(room1.getId()))
                .body("name", equalTo(room1.getName()));
    }

    @Test
    public void getRoomByName() {

        ArrayList<Room> roomArray = new ArrayList<>();
        Room room1 = new Room(1, "room1");
        Room room2 = new Room(1, "room1");
        roomArray.add(room1);
        roomArray.add(room2);

        when(roomController.getRoomByNameId("room1")).thenReturn(roomArray);

        Room[] rooms = get(uri + "/api/rooms/Name/room1").then()
                .statusCode(200)
                .extract()
                .as(Room[].class);
        assertEquals(rooms.length, 2);
    }

    @Test
    public void createRoom() {

        Room room1 = new Room(1, "room1");

        when(roomController.createRoom(room1)).thenReturn(ResponseEntity.ok(room1));

        String requestBody = "{\"name\":\"room1\"}";

        given()
                .contentType(ContentType.JSON)
                .body(requestBody)
                .when()
                .post(uri + "/api/rooms")
                .then()
                .statusCode(200);
    }

    @Test
    public void deleteReservations() {
        when(roomController.deleteRoom(1)).thenReturn(ResponseEntity.ok().build());
        when(roomController.deleteRoom(2)).thenReturn(ResponseEntity.notFound().build());

        delete(uri + "/api/rooms/1").then().statusCode(200);
        delete(uri + "/api/rooms/2").then().statusCode(404);
    }
}
