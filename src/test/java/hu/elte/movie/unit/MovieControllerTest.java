package hu.elte.movie.unit;

import hu.elte.movie.controller.MovieController;
import hu.elte.movie.model.Movie;
import io.restassured.http.ContentType;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.PostConstruct;
import java.util.ArrayList;

import static io.restassured.RestAssured.*;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class MovieControllerTest {

    @LocalServerPort
    private int port;

    private String uri;

    @PostConstruct
    public void init() {
        uri = "http://localhost:" + port;
    }

    @MockBean
    MovieController movieController;

    @Test
    public void getAllMovies() {

        ArrayList<Movie> movieArray = new ArrayList<>();
        Movie testMovie = new Movie(1, "title", "2018");
        Movie testMovie2 = new Movie(2, "title2", "2019");
        Movie testMovie3 = new Movie(3, "title3", "2020");
        movieArray.add(testMovie);
        movieArray.add(testMovie2);
        movieArray.add(testMovie3);
        when(movieController.getMovies()).thenReturn(movieArray);

        Movie[] movies = get(uri + "/api/movies").then()
                .statusCode(200)
                .extract()
                .as(Movie[].class);
        assertEquals(movies.length, 3);
    }

    @Test
    public void getMovieById() {

        Movie testMovie = new Movie(1, "title", "2018");
        when(movieController.getMovieById(1)).thenReturn(java.util.Optional.of(testMovie));

        get(uri + "/api/movies/1").then()
                .assertThat()
                .statusCode(HttpStatus.OK.value())
                .body("id", equalTo(testMovie.getId()))
                .body("title", equalTo(testMovie.getTitle()))
                .body("releaseYear", equalTo(testMovie.getReleaseYear()));
    }

    @Test
    public void getMovieByTitle() {
        ArrayList<Movie> movieArray = new ArrayList<>();
        Movie testMovie = new Movie(1, "title", "2018");
        Movie testMovie2 = new Movie(2, "title", "2020");
        movieArray.add(testMovie);
        movieArray.add(testMovie2);
        when(movieController.getMovieByTitle("title")).thenReturn(movieArray);

        Movie[] movies = get(uri + "/api/movies/title/title").then()
                .statusCode(200)
                .extract()
                .as(Movie[].class);
        assertEquals(movies.length, 2);
    }

    @Test
    public void getMovieByReleaseYear() {
        ArrayList<Movie> movieArray = new ArrayList<>();
        Movie testMovie = new Movie(1, "title", "2020");
        Movie testMovie2 = new Movie(2, "title", "2020");
        movieArray.add(testMovie);
        movieArray.add(testMovie2);
        when(movieController.getMovieByReleaseYear("2020")).thenReturn(movieArray);

        Movie[] movies = get(uri + "/api/movies/release-year/2020").then()
                .statusCode(200)
                .extract()
                .as(Movie[].class);
        assertEquals(movies.length, 2);
    }

    @Test
    public void createMovie() {
        Movie testMovie = new Movie(1, "title", "2020");

        when(movieController.createMovie(testMovie)).thenReturn(ResponseEntity.ok(testMovie));

        String requestBody = "{\"title\":\"title\",\"releaseYear\":\"2020\"}";

        given()
                .contentType(ContentType.JSON)
                .body(requestBody)
                .when()
                .post(uri + "/api/movies")
                .then()
                .statusCode(200);
    }

    @Test
    public void deleteMovie() {
        when(movieController.deleteMovie(2)).thenReturn(ResponseEntity.ok().build());
        when(movieController.deleteMovie(3)).thenReturn(ResponseEntity.notFound().build());

        delete(uri + "/api/movies/2").then().statusCode(200);
        delete(uri + "/api/movies/3").then().statusCode(404);
    }


}
