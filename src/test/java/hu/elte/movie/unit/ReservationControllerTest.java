package hu.elte.movie.unit;

import hu.elte.movie.controller.ReservationController;
import hu.elte.movie.model.*;
import io.restassured.http.ContentType;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.PostConstruct;
import java.util.ArrayList;

import static io.restassured.RestAssured.*;
import static io.restassured.RestAssured.delete;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@RunWith(SpringRunner.class)
public class ReservationControllerTest {

    @LocalServerPort
    private int port;

    private String uri;

    @PostConstruct
    public void init() {
        uri = "http://localhost:" + port;
    }

    @MockBean
    ReservationController reservationController;

    @Test
    public void getAllReservations() {

        ArrayList<Reservation> reservationArray = new ArrayList<>();
        Movie movie1 = new Movie(1,"title1", "2017");
        Movie movie2 = new Movie(2, "title2", "2018");
        Movie movie3 = new Movie(3, "title3", "2029");
        Room room1 = new Room(1, "room1");
        Room room2 = new Room(2, "room2");
        Room room3 = new Room(3, "room3");
        Reservation testReservation = new Reservation(1,
                new User(1,"name1", "pass1", User.Role.USER) ,
                new Screening(1, movie1, room1, 1000, "12:30"));
        Reservation testReservation2 = new Reservation(2,
                new User(2,"name2", "pass2", User.Role.USER) ,
                new Screening(2, movie2, room2, 2000, "13:30"));
        Reservation testReservation3 = new Reservation(3,
                new User(3,"name3", "pass3", User.Role.USER) ,
                new Screening(3, movie3, room3, 3000, "14:30"));
        reservationArray.add(testReservation);
        reservationArray.add(testReservation2);
        reservationArray.add(testReservation3);
        when(reservationController.getReservations()).thenReturn(reservationArray);

        Reservation[] reservations = get(uri + "/api/reservations").then()
                .statusCode(200)
                .extract()
                .as(Reservation[].class);
        assertEquals(reservations.length, 3);
    }

    @Test
    public void getReservationById() {

        Movie movie1 = new Movie(1,"title1", "2017");
        Room room1 = new Room(1, "room1");
        User user1 = new User(1,"name1", "pass1", User.Role.USER);
        Screening screening1 = new Screening(1, movie1, room1, 1000, "12:30");
        Reservation testReservation = new Reservation(1, user1 , screening1);

        when(reservationController.getReservationById(1)).thenReturn(java.util.Optional.of(testReservation));

        get(uri + "/api/reservations/1").then()
                .assertThat()
                .statusCode(HttpStatus.OK.value())
                .body("id", equalTo(testReservation.getId()))
                .body("user.id", equalTo(testReservation.getUser().getId()))
                .body("user.username", equalTo(testReservation.getUser().getUsername()))
                .body("user.password", equalTo(testReservation.getUser().getPassword()))
                .body("user.role", equalTo(testReservation.getUser().getRole().toString()))
                .body("screening.id", equalTo(testReservation.getScreening().getId()))
                .body("screening.movie.id", equalTo(testReservation.getScreening().getMovie().getId()))
                .body("screening.movie.title", equalTo(testReservation.getScreening().getMovie().getTitle()))
                .body("screening.movie.releaseYear", equalTo(testReservation.getScreening().getMovie().getReleaseYear()))
                .body("screening.room.id", equalTo(testReservation.getScreening().getRoom().getId()))
                .body("screening.room.name", equalTo(testReservation.getScreening().getRoom().getName()))
                .body("screening.price", equalTo(testReservation.getScreening().getPrice()))
                .body("screening.time", equalTo(testReservation.getScreening().getTime()))
        ;
    }
    @Test
    public void getByUserId() {
        ArrayList<Reservation> reservationArray = new ArrayList<>();
        Movie movie1 = new Movie(1,"title1", "2017");
        Movie movie2 = new Movie(2, "title2", "2018");
        Room room1 = new Room(1, "room1");
        Room room2 = new Room(2, "room2");
        Reservation testReservation = new Reservation(1,
                new User(1,"name1", "pass1", User.Role.USER) ,
                new Screening(1, movie1, room1, 1000, "12:30"));
        Reservation testReservation2 = new Reservation(2,
                new User(1,"name2", "pass2", User.Role.USER) ,
                new Screening(2, movie2, room2, 2000, "13:30"));
        reservationArray.add(testReservation);
        reservationArray.add(testReservation2);
        when(reservationController.getReservationByUserId(1)).thenReturn(reservationArray);

        Reservation[] reservations = get(uri + "/api/reservations/userId/1").then()
                .statusCode(200)
                .extract()
                .as(Reservation[].class);
        assertEquals(reservations.length, 2);
    }
    @Test
    public void getByScreeningId() {
        ArrayList<Reservation> reservationArray = new ArrayList<>();
        Movie movie1 = new Movie(1,"title1", "2017");
        Movie movie2 = new Movie(2, "title2", "2018");
        Room room1 = new Room(1, "room1");
        Room room2 = new Room(2, "room2");
        Reservation testReservation = new Reservation(1,
                new User(1,"name1", "pass1", User.Role.USER) ,
                new Screening(1, movie1, room1, 1000, "12:30"));
        Reservation testReservation2 = new Reservation(2,
                new User(2,"name2", "pass2", User.Role.USER) ,
                new Screening(1, movie2, room2, 2000, "13:30"));
        reservationArray.add(testReservation);
        reservationArray.add(testReservation2);
        when(reservationController.getReservationByScreeningId(1)).thenReturn(reservationArray);

        Reservation[] reservations = get(uri + "/api/reservations/screeningId/1").then()
                .statusCode(200)
                .extract()
                .as(Reservation[].class);
        assertEquals(reservations.length, 2);
    }

    @Test
    public void createReservation() {
        Movie movie1 = new Movie(1,"title1", "2017");
        Room room1 = new Room(1, "room1");
        Reservation testReservation = new Reservation(1,
                new User(1,"name1", "pass1", User.Role.USER) ,
                new Screening(1, movie1, room1, 1000, "12:30"));

        when(reservationController.createReservation(testReservation)).thenReturn(ResponseEntity.ok(testReservation));

        String requestBody = "{\"user\":{\"id\":\"1\",\"username\":\"name1\",\"password\":\"pass1\",\"Role\":\"User\"}," +
                "\"screening\":{\"id\":\"1\",\"movie\":{\"id\":\"1\",\"title\":\"title1\",\"releaseYear\":\"2017\"},\"room\":{\"id\":\"1\",\"name\":\"room1\"},\"price\":\"1000\",\"time\":\"12:30\"}}";

        given()
                .contentType(ContentType.JSON)
                .body(requestBody)
                .when()
                .post(uri + "/api/reservations")
                .then()
                .statusCode(200);
    }

    @Test
    public void deleteReservations() {
       when(reservationController.deleteReservation(1)).thenReturn(ResponseEntity.ok().build());
        when(reservationController.deleteReservation(2)).thenReturn(ResponseEntity.notFound().build());

        delete(uri + "/api/reservations/1").then().statusCode(200);
        delete(uri + "/api/reservations/2").then().statusCode(404);
    }
}
