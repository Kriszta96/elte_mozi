package hu.elte.movie.unit;

import hu.elte.movie.controller.UserController;
import hu.elte.movie.model.*;
import io.restassured.http.ContentType;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.PostConstruct;
import java.util.ArrayList;

import static io.restassured.RestAssured.*;
import static io.restassured.RestAssured.delete;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@RunWith(SpringRunner.class)
public class UserControllerTest {

    @LocalServerPort
    private int port;

    private String uri;

    @PostConstruct
    public void init() {
        uri = "http://localhost:" + port;
    }

    @MockBean
    UserController userController;

    @Test
    public void getAllUsers() {

        ArrayList<User> userArray = new ArrayList<>();
        User user1 = new User(1, "Bela", "asd123", User.Role.USER);
        User user2 = new User(2, "Dezso", "fgh456", User.Role.OPERATOR);
        User user3 = new User(3, "Geza", "valami123", User.Role.USER);
        userArray.add(user1);
        userArray.add(user2);
        userArray.add(user3);
        when(userController.getUsers()).thenReturn(userArray);

        User[] users = get(uri + "/api/users").then()
                .statusCode(200)
                .extract()
                .as(User[].class);
        assertEquals(users.length, 3);
    }

    @Test
    public void getUserById() {

        User user1 = new User(1, "Bela", "asd123", User.Role.USER);
        when(userController.getUserById(1)).thenReturn(java.util.Optional.of(user1));

        get(uri + "/api/users/1").then()
                .assertThat()
                .statusCode(HttpStatus.OK.value())
                .body("id", equalTo(user1.getId()))
                .body("username", equalTo(user1.getUsername()))
                .body("password", equalTo(user1.getPassword()))
                .body("role", equalTo(user1.getRole().toString()));
    }

    @Test
    public void getUserByUsername() {

        User user1 = new User(1, "Bela", "asd123", User.Role.USER);
        when(userController.getUserByUsername("Bela")).thenReturn(ResponseEntity.ok(user1));

        get(uri + "/api/users/username/Bela").then()
                .assertThat()
                .statusCode(HttpStatus.OK.value())
                .body("id", equalTo(user1.getId()))
                .body("username", equalTo(user1.getUsername()))
                .body("password", equalTo(user1.getPassword()))
                .body("role", equalTo(user1.getRole().toString()));
    }

    @Test
    public void createUser() {

        User user1 = new User(1, "Bela", "asd123", User.Role.USER);

        when(userController.createUser(user1)).thenReturn(ResponseEntity.ok(user1));

        String requestBody = "{\"username\":\"Bela\",\"password\":\"asd123\",\"role\":\"USER\"}";

        given()
                .contentType(ContentType.JSON)
                .body(requestBody)
                .when()
                .post(uri + "/api/users")
                .then()
                .statusCode(200);
    }

    @Test
    public void modifyUser() {

        User user1 = new User(1, "Bela", "asd123", User.Role.USER);
        User user2 = new User(2, "Dezso", "fgh456", User.Role.OPERATOR);

        when(userController.modifyUser(1, user2)).thenReturn(ResponseEntity.ok(user1));

        String requestBody = "{\"username\":\"Dezso\",\"password\":\"fgh456\",\"role\":\"OPERATOR\"}";

        given()
                .contentType(ContentType.JSON)
                .body(requestBody)
                .when()
                .post(uri + "/api/users")
                .then()
                .statusCode(200);


        when(userController.modifyUser(3, user2)).thenReturn(ResponseEntity.notFound().build());

        given()
                .contentType(ContentType.JSON)
                .then()
                .statusCode(404);
    }

    @Test
    public void deleteUser() {
        when(userController.deleteUser(1)).thenReturn(ResponseEntity.ok().build());
        when(userController.deleteUser(2)).thenReturn(ResponseEntity.notFound().build());

        delete(uri + "/api/users/1").then().statusCode(200);
        delete(uri + "/api/users/2").then().statusCode(404);
    }
}
