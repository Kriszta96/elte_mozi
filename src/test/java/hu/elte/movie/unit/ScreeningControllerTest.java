package hu.elte.movie.unit;

import hu.elte.movie.controller.ScreeningController;
import hu.elte.movie.model.*;
import io.restassured.http.ContentType;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.PostConstruct;
import java.util.ArrayList;

import static io.restassured.RestAssured.*;
import static io.restassured.RestAssured.delete;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@RunWith(SpringRunner.class)
public class ScreeningControllerTest {

    @LocalServerPort
    private int port;

    private String uri;

    @PostConstruct
    public void init() {
        uri = "http://localhost:" + port;
    }

    @MockBean
    ScreeningController screeningController;

    @Test
    public void getAllReservations() {

        ArrayList<Screening> screeningArray = new ArrayList<>();
        Movie movie1 = new Movie(1, "title1", "2017");
        Movie movie2 = new Movie(2, "title2", "2018");
        Movie movie3 = new Movie(3, "title3", "2029");
        Room room1 = new Room(1, "room1");
        Room room2 = new Room(2, "room2");
        Room room3 = new Room(3, "room3");
        Screening screening1 = new Screening(1, movie1, room1, 1000, "12:30");
        Screening screening2 = new Screening(2, movie2, room2, 2000, "13:30");
        Screening screening3 = new Screening(3, movie3, room3, 3000, "14:30");
        screeningArray.add(screening1);
        screeningArray.add(screening2);
        screeningArray.add(screening3);
        when(screeningController.getScreenings()).thenReturn(screeningArray);

        Screening[] screenings = get(uri + "/api/screenings").then()
                .statusCode(200)
                .extract()
                .as(Screening[].class);
        assertEquals(screenings.length, 3);
    }

    @Test
    public void getScreeningById() {

        Movie movie1 = new Movie(1, "title1", "2017");
        Room room1 = new Room(1, "room1");
        Screening screening1 = new Screening(1, movie1, room1, 1000, "12:30");

        when(screeningController.getScreeningById(1)).thenReturn(java.util.Optional.of(screening1));

        get(uri + "/api/screenings/1").then()
                .assertThat()
                .statusCode(HttpStatus.OK.value())
                .body("id", equalTo(screening1.getId()))
                .body("movie.id", equalTo(screening1.getMovie().getId()))
                .body("movie.title", equalTo(screening1.getMovie().getTitle()))
                .body("movie.releaseYear", equalTo(screening1.getMovie().getReleaseYear()))
                .body("room.id", equalTo(screening1.getRoom().getId()))
                .body("room.name", equalTo(screening1.getRoom().getName()))
                .body("price", equalTo(screening1.getPrice()))
                .body("time", equalTo(screening1.getTime()));
    }

    @Test
    public void getScreeningByMovieId() {
        ArrayList<Screening> screeningArray = new ArrayList<>();
        Movie movie1 = new Movie(1, "title1", "2017");
        Movie movie2 = new Movie(1, "title2", "2018");
        Room room1 = new Room(1, "room1");
        Room room2 = new Room(2, "room2");
        Screening screening1 = new Screening(1, movie1, room1, 1000, "12:30");
        Screening screening2 = new Screening(2, movie2, room2, 2000, "13:30");
        screeningArray.add(screening1);
        screeningArray.add(screening2);
        when(screeningController.getScreeningByMovieId(1)).thenReturn(screeningArray);

        Screening[] screenings = get(uri + "/api/screenings/movieId/1").then()
                .statusCode(200)
                .extract()
                .as(Screening[].class);
        assertEquals(screenings.length, 2);
    }

    @Test
    public void getScreeningByRoomId() {
        ArrayList<Screening> screeningArray = new ArrayList<>();
        Movie movie1 = new Movie(1, "title1", "2017");
        Movie movie2 = new Movie(2, "title2", "2018");
        Room room1 = new Room(1, "room1");
        Room room2 = new Room(1, "room2");
        Screening screening1 = new Screening(1, movie1, room1, 1000, "12:30");
        Screening screening2 = new Screening(2, movie2, room2, 2000, "13:30");
        screeningArray.add(screening1);
        screeningArray.add(screening2);
        when(screeningController.getScreeningByRoomId(1)).thenReturn(screeningArray);

        Screening[] screenings = get(uri + "/api/screenings/roomID/1").then()
                .statusCode(200)
                .extract()
                .as(Screening[].class);
        assertEquals(screenings.length, 2);
    }

    @Test
    public void getScreeningByTime() {
        ArrayList<Screening> screeningArray = new ArrayList<>();
        Movie movie1 = new Movie(1, "title1", "2017");
        Movie movie2 = new Movie(2, "title2", "2018");
        Room room1 = new Room(1, "room1");
        Room room2 = new Room(2, "room2");
        Screening screening1 = new Screening(1, movie1, room1, 1000, "13:30");
        Screening screening2 = new Screening(2, movie2, room2, 2000, "13:30");
        screeningArray.add(screening1);
        screeningArray.add(screening2);
        when(screeningController.getScreeningByTime("13:30")).thenReturn(screeningArray);

        Screening[] screenings = get(uri + "/api/screenings/time/13:30").then()
                .statusCode(200)
                .extract()
                .as(Screening[].class);
        assertEquals(screenings.length, 2);
    }

    @Test
    public void createScreening() {
        Movie movie1 = new Movie(1, "title1", "2017");
        Room room1 = new Room(1, "room1");
        Screening screening1 = new Screening(1, movie1, room1, 1000, "13:30");

        when(screeningController.createScreening(screening1)).thenReturn(ResponseEntity.ok(screening1));

        String requestBody = "{\"movie\":{\"id\":\"1\",\"title\":\"title1\",\"releaseYear\":\"2017\"},\"room\":{\"id\":\"1\",\"name\":\"room1\"},\"price\":\"1000\",\"time\":\"13:30\"}";

        given()
                .contentType(ContentType.JSON)
                .body(requestBody)
                .when()
                .post(uri + "/api/screenings")
                .then()
                .statusCode(200);
    }

    @Test
    public void deleteScreening() {
        when(screeningController.deleteScreening(1)).thenReturn(ResponseEntity.ok().build());
        when(screeningController.deleteScreening(2)).thenReturn(ResponseEntity.notFound().build());

        delete(uri + "/api/screenings/1").then().statusCode(200);
        delete(uri + "/api/screenings/2").then().statusCode(404);
    }
}
