package hu.elte.movie.integration;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
@TestPropertySource(locations = "classpath:application-integrationtest.properties")
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)

public class RoomIT {
    @Autowired
    private MockMvc mvc;

    @Test
    public void getRoomById() throws Exception {
        int ROOM_ID = 1;

        mvc.perform(MockMvcRequestBuilders.get("/api/rooms/{id}", ROOM_ID)
                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").value(ROOM_ID));
    }

    @Test
    public void getAllRooms() throws Exception {
        int NUMBER_OF_ROOMS = 2;

        mvc.perform(MockMvcRequestBuilders.get("/api/rooms")
                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$", hasSize(NUMBER_OF_ROOMS)));
    }

    @Test
    public void getRoomByName() throws Exception {
        String ROOM_NAME = "Comfy Room";

        mvc.perform(MockMvcRequestBuilders.get("/api/rooms/Name/{name}", ROOM_NAME)
                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].name").value(ROOM_NAME));
    }

    @Test
    public void createRooms() throws Exception {
        String ROOM = "{\"Name\": \"Test Name\"}";

        mvc.perform(MockMvcRequestBuilders.post("/api/rooms")
                .contentType(MediaType.APPLICATION_JSON)
                .content(ROOM)
                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    public void deleteRoom() throws Exception {
        int ROOM_ID = 1;

        mvc.perform(MockMvcRequestBuilders.delete("/api/rooms/{id}", ROOM_ID)
                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());
    }
}
