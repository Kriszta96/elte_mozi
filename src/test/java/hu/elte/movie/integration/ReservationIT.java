package hu.elte.movie.integration;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
@TestPropertySource(locations = "classpath:application-integrationtest.properties")
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)

public class ReservationIT {
    @Autowired
    private MockMvc mvc;

    @Test
    public void getReservationById() throws Exception {
        int RESERVATION_ID = 1;

        mvc.perform(MockMvcRequestBuilders.get("/api/reservations/{id}", RESERVATION_ID)
                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").value(RESERVATION_ID));
    }

    @Test
    public void getAllReservations() throws Exception {
        int NUMBER_OF_RESERVATIONS = 5;

        mvc.perform(MockMvcRequestBuilders.get("/api/reservations")
                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$", hasSize(NUMBER_OF_RESERVATIONS)));
    }

    @Test
    public void getReservationByUserId() throws Exception {
        String USER_ID = "1";

        mvc.perform(MockMvcRequestBuilders.get("/api/reservations/userId/{userId}", USER_ID)
                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].user.id").value(USER_ID));
    }

    @Test
    public void getReservationByScreeningId() throws Exception {
        String SCREENING_ID = "1";

        mvc.perform(MockMvcRequestBuilders.get("/api/reservations/screeningId/{screeningId}", SCREENING_ID)
                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].screening.id").value(SCREENING_ID));
    }

    @Test
    public void createReservation() throws Exception {
        String RESERVATION = "{\"userId\": \"2\", \"screeningId\": \"2\"}";

        mvc.perform(MockMvcRequestBuilders.post("/api/reservations")
                .contentType(MediaType.APPLICATION_JSON)
                .content(RESERVATION)
                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    public void deleteReservations() throws Exception {
        int RESERVATION_ID = 1;

        mvc.perform(MockMvcRequestBuilders.delete("/api/reservations/{id}", RESERVATION_ID)
                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());
    }
}
