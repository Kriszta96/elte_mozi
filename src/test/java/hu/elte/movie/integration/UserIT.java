package hu.elte.movie.integration;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
@TestPropertySource(locations = "classpath:application-integrationtest.properties")
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
public class UserIT {

    @Autowired
    private MockMvc mvc;

    @Test
    public void getUserById() throws Exception {
        int USER_ID = 1;

        mvc.perform(MockMvcRequestBuilders.get("/api/users/{id}", USER_ID)
                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").value(USER_ID));
    }

    @Test
    public void getAllUsers() throws Exception {
        int NUMBER_OF_USERS = 4;

        mvc.perform(MockMvcRequestBuilders.get("/api/users")
                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$", hasSize(NUMBER_OF_USERS)));
    }

    @Test
    public void getUserByUsername() throws Exception {
        String USERNAME = "gabor";

        mvc.perform(MockMvcRequestBuilders.get("/api/users/username/{username}", USERNAME)
                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.username").value(USERNAME));
    }

    @Test
    public void validLogin() throws Exception {
        String USERNAME = "gabor";

        mvc.perform(MockMvcRequestBuilders.post("/api/users/login")
                .contentType(MediaType.APPLICATION_JSON)
                .content(USERNAME)
                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    public void invalidLogin() throws Exception {
        String USERNAME = "r5tgvxlkckl";

        mvc.perform(MockMvcRequestBuilders.post("/api/users/login")
                .contentType(MediaType.APPLICATION_JSON)
                .content(USERNAME)
                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isBadRequest());
    }

    @Test
    public void validCreateUser() throws Exception {
        String USER = "{\"username\": \"Test User\", \"password\" : \"fbvdfn543543jk\"}";

        mvc.perform(MockMvcRequestBuilders.post("/api/users")
                .contentType(MediaType.APPLICATION_JSON)
                .content(USER)
                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    public void invalidCreateUser() throws Exception {
        String USER = "{\"username\": \"gabor\", \"password\" : \"fbvdfn543543jk\"}";

        mvc.perform(MockMvcRequestBuilders.post("/api/users")
                .contentType(MediaType.APPLICATION_JSON)
                .content(USER)
                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isBadRequest());
    }

    @Test
    public void validUpdateUser() throws Exception {
        int USER_ID = 2;
        String NEW_USER = "{\"username\": \"New Name\"}";

        mvc.perform(MockMvcRequestBuilders.put("/api/users/{id}", USER_ID)
                .contentType(MediaType.APPLICATION_JSON)
                .content(NEW_USER)
                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    public void invalidUpdateUser() throws Exception {
        int USER_ID = 2;
        String NEW_USER = "{\"username\": \"kriszti\"}";

        mvc.perform(MockMvcRequestBuilders.put("/api/users/{id}", USER_ID)
                .contentType(MediaType.APPLICATION_JSON)
                .content(NEW_USER)
                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isBadRequest());
    }

    @Test
    public void deleteUser() throws Exception {
        int USER_ID = 2;

        mvc.perform(MockMvcRequestBuilders.delete("/api/users/{id}", USER_ID)
                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());
    }

}
