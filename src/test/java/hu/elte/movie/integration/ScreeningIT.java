package hu.elte.movie.integration;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
@TestPropertySource(locations = "classpath:application-integrationtest.properties")
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
public class ScreeningIT {

    @Autowired
    private MockMvc mvc;

    @Test
    public void getScreeningById() throws Exception {
        int SCREENING_ID = 1;

        mvc.perform(MockMvcRequestBuilders.get("/api/screenings/{id}", SCREENING_ID)
                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").value(SCREENING_ID));
    }

    @Test
    public void getAllScreenings() throws Exception {
        int NUMBER_OF_SCREENINGS = 3;

        mvc.perform(MockMvcRequestBuilders.get("/api/screenings")
                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$", hasSize(NUMBER_OF_SCREENINGS)));
    }

    @Test
    public void getScreeningByMovieId() throws Exception {
        int MOVIE_ID = 1;

        mvc.perform(MockMvcRequestBuilders.get("/api/screenings/movieId/{movieId}", MOVIE_ID)
                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].movie.id").value(MOVIE_ID));
    }

    @Test
    public void getScreeningByRoomId() throws Exception {
        int ROOM_ID = 1;

        mvc.perform(MockMvcRequestBuilders.get("/api/screenings/roomID/{roomId}", ROOM_ID)
                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].room.id").value(ROOM_ID));
    }

    @Test
    public void getScreeningByTime() throws Exception {
        String SCREENING_TIME = "18:00";

        mvc.perform(MockMvcRequestBuilders.get("/api/screenings/time/{time}", SCREENING_TIME)
                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].time").value(SCREENING_TIME));
    }

    @Test
    public void createScreening() throws Exception {
        String SCREENING = "{\"movie_id\": \"1\", \"room_id\": \"1\", \"price\": 1000, \"time\": \"13:30\"}";


        mvc.perform(MockMvcRequestBuilders.post("/api/screenings")
                .contentType(MediaType.APPLICATION_JSON)
                .content(SCREENING)
                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    public void deleteScreening() throws Exception {
        int SCREENING_ID = 1;

        mvc.perform(MockMvcRequestBuilders.delete("/api/screenings/{id}", SCREENING_ID)
                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());
    }
}
