package hu.elte.movie.integration;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
@TestPropertySource(locations = "classpath:application-integrationtest.properties")
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
public class MovieIT {

    @Autowired
    private MockMvc mvc;

    @Test
    public void getMovieById() throws Exception {
        int MOVIE_ID = 1;

        mvc.perform(MockMvcRequestBuilders.get("/api/movies/{id}", MOVIE_ID)
                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").value(MOVIE_ID));
    }

    @Test
    public void getAllMovies() throws Exception {
        int NUMBER_OF_MOVIES = 3;

        mvc.perform(MockMvcRequestBuilders.get("/api/movies")
                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$", hasSize(NUMBER_OF_MOVIES)));
    }

    @Test
    public void getMovieByTitle() throws Exception {
        String MOVIE_TITLE = "Alkonyat";

        mvc.perform(MockMvcRequestBuilders.get("/api/movies/title/{title}", MOVIE_TITLE)
                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].title").value(MOVIE_TITLE));
    }

    @Test
    public void getMovieByReleaseYear() throws Exception {
        String MOVIE_RELEASE_YEAR = "2008";

        mvc.perform(MockMvcRequestBuilders.get("/api/movies/release-year/{releaseYear}", MOVIE_RELEASE_YEAR)
                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].releaseYear").value(MOVIE_RELEASE_YEAR));
    }

    @Test
    public void createMovie() throws Exception {
        String MOVIE = "{\"title\": \"Test Title\", \"releaseYear\": \"2018\"}";

        mvc.perform(MockMvcRequestBuilders.post("/api/movies")
                .contentType(MediaType.APPLICATION_JSON)
                .content(MOVIE)
                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    public void deleteMovie() throws Exception {
        int MOVIE_ID = 1;

        mvc.perform(MockMvcRequestBuilders.delete("/api/movies/{id}", MOVIE_ID)
                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());
    }
}


