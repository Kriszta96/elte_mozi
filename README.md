# ELTE_MOZI

**Mozijegy foglaló**

backend: Spring
adatbázis: h2
frontend: Angular
 
Adatbázis táblák:
Film, Terem, Foglalás, Vetítés, User
Funkciók:

felhasználó, regisztráció, profil, foglalásaim,
keresés: filmcím, időpont, helyszín, műfaj,
foglalás: foglalást hozzárendeljük a felhasználóhoz
Frontend: Material, Material Icons, Bootstrap

**Tesztek**

A tesztek útvonalai:
* backend: src/test/java/hu/elte/movie
* frontend: src/main/frontend/src/app -> spec.ts fájlok

A CI-hoz a Gitlab-ot használjuk, gitlab-ci.yml fájl megírásával, ezután a Gitlab docker image-el buildeli és futtatja le a teszteket.
A frontend és backend teszteket egyaránt a Maven kezeli, először a unit teszteket futtatja le, majd az integrációs teszteket.
Két dependency ad hozzá plusz funkciókat a teszteléshez:
* frontend-maven-plugin: ez gondoskodik arról, hogy a maven meghívja az angular teszteket az "ng test-ci"-al
* maven-failsafe-plugin: ha az integrációs tesztek elromlanak, akkor biztonságosan építse le a teszt környezetet.

A teljes teszt fázist a "maven verify" meghívásával lehet lefuttatni.

**Eszközök**

Két eszköz fut le a CI alatt, ha bármelyik hibát ad, eltöri az egész pipeline-t.
* Lint: Java és Typescript
* Javadoc: a generált javadoc a https://kriszta96.gitlab.io/elte_mozi oldalon érhető el